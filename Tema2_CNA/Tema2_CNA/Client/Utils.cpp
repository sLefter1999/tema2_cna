#include "Utils.h"

#include <regex>
#include <sstream>

bool Utils::CheckDate(const std::string& date)
{

    // Here b is an object of regex (regular expression) 
    std::regex b("\\b\\d{1,2}[/]\\d{1,2}[/]\\d{4}\\b");

    // regex_match function matches string a against regex b 
    if (regex_match(date, b))
    {
        std::stringstream buffer(date);

        std::string date;

        std::getline(buffer, date, '/');
        int month = std::stoi(date);

        std::getline(buffer, date, '/');
        int day = std::stoi(date);

        std::getline(buffer, date, '/');
        int year = std::stoi(date);

        if (1000 <= year <= 3000)
        {
            if ((month == 1 || month == 3 || month == 5 || month == 7 || month == 8 || month == 10 || month == 12) && day > 0 && day <= 31)
                return true;
            else
                if (month == 4 || month == 6 || month == 9 || month == 11 && day > 0 && day <= 30)
                    return true;
                else
                    if (month == 2)
                    {
                        if ((year % 400 == 0 || (year % 100 != 0 && year % 4 == 0)) && day > 0 && day <= 29)
                            return true;
                        else if (day > 0 && day <= 28)
                            return true;
                        else
                            return false;
                    }
                    else
                        return false;
        }
        else
            return false;
    }
    return false;
}

