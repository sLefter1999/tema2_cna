#include <iostream>
#include <grpc/grpc.h>
#include <grpcpp/channel.h>
#include <grpcpp/client_context.h>
#include <grpcpp/create_channel.h>
#include <grpcpp/security/credentials.h>

#include "SignOperation.grpc.pb.h"
#include "WinterSignOperation.grpc.pb.h"
#include "SpringSignOperation.grpc.pb.h"
#include "SummerSignOperation.grpc.pb.h"
#include "AutumnSignOperation.grpc.pb.h"

#include "Utils.h"

#include <string>
#include <ctime>
#include <regex>
#include <sstream>

using grpc::Channel;
using grpc::ClientContext;
using grpc::ClientReader;
using grpc::ClientReaderWriter;
using grpc::ClientWriter;


int main()
{
    grpc_init();

   std::shared_ptr<grpc_impl::Channel> channel = grpc::CreateChannel("localhost:8888", grpc::InsecureChannelCredentials());

   std::string date;

    do 
    {
        std::cout << "Introduceti data de nastere:";
        std::cin >> date;

        while (!Utils::CheckDate(date))
        {
            std::cout << "Data invalida" << std::endl;
            std::cout << "Introduceti o data corecta" << std::endl;
            std::cin >> date;
        }

        ClientContext context;
        SignResponse response;
        grpc::Status status;
        SignRequest nameRequest;

        nameRequest.set_birthdate(date);

        auto sign_stub = GetSignService::NewStub(channel);
        status = sign_stub->GetSign(&context,nameRequest,&response);

        if (status.ok())
        {
            std::cout << response.sign() << std::endl;
        }

    } while (date != "exit");
}

