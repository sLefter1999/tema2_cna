// Generated by the gRPC C++ plugin.
// If you make any local change, they will be lost.
// source: SpringSignOperation.proto
#ifndef GRPC_SpringSignOperation_2eproto__INCLUDED
#define GRPC_SpringSignOperation_2eproto__INCLUDED

#include "SpringSignOperation.pb.h"

#include <functional>
#include <grpc/impl/codegen/port_platform.h>
#include <grpcpp/impl/codegen/async_generic_service.h>
#include <grpcpp/impl/codegen/async_stream.h>
#include <grpcpp/impl/codegen/async_unary_call.h>
#include <grpcpp/impl/codegen/client_callback.h>
#include <grpcpp/impl/codegen/client_context.h>
#include <grpcpp/impl/codegen/completion_queue.h>
#include <grpcpp/impl/codegen/message_allocator.h>
#include <grpcpp/impl/codegen/method_handler.h>
#include <grpcpp/impl/codegen/proto_utils.h>
#include <grpcpp/impl/codegen/rpc_method.h>
#include <grpcpp/impl/codegen/server_callback.h>
#include <grpcpp/impl/codegen/server_callback_handlers.h>
#include <grpcpp/impl/codegen/server_context.h>
#include <grpcpp/impl/codegen/service_type.h>
#include <grpcpp/impl/codegen/status.h>
#include <grpcpp/impl/codegen/stub_options.h>
#include <grpcpp/impl/codegen/sync_stream.h>

class GetSpringSignService final {
 public:
  static constexpr char const* service_full_name() {
    return "GetSpringSignService";
  }
  class StubInterface {
   public:
    virtual ~StubInterface() {}
    virtual ::grpc::Status GetSign(::grpc::ClientContext* context, const ::SignRequest& request, ::SignResponse* response) = 0;
    std::unique_ptr< ::grpc::ClientAsyncResponseReaderInterface< ::SignResponse>> AsyncGetSign(::grpc::ClientContext* context, const ::SignRequest& request, ::grpc::CompletionQueue* cq) {
      return std::unique_ptr< ::grpc::ClientAsyncResponseReaderInterface< ::SignResponse>>(AsyncGetSignRaw(context, request, cq));
    }
    std::unique_ptr< ::grpc::ClientAsyncResponseReaderInterface< ::SignResponse>> PrepareAsyncGetSign(::grpc::ClientContext* context, const ::SignRequest& request, ::grpc::CompletionQueue* cq) {
      return std::unique_ptr< ::grpc::ClientAsyncResponseReaderInterface< ::SignResponse>>(PrepareAsyncGetSignRaw(context, request, cq));
    }
    class experimental_async_interface {
     public:
      virtual ~experimental_async_interface() {}
      virtual void GetSign(::grpc::ClientContext* context, const ::SignRequest* request, ::SignResponse* response, std::function<void(::grpc::Status)>) = 0;
      virtual void GetSign(::grpc::ClientContext* context, const ::grpc::ByteBuffer* request, ::SignResponse* response, std::function<void(::grpc::Status)>) = 0;
      #ifdef GRPC_CALLBACK_API_NONEXPERIMENTAL
      virtual void GetSign(::grpc::ClientContext* context, const ::SignRequest* request, ::SignResponse* response, ::grpc::ClientUnaryReactor* reactor) = 0;
      #else
      virtual void GetSign(::grpc::ClientContext* context, const ::SignRequest* request, ::SignResponse* response, ::grpc::experimental::ClientUnaryReactor* reactor) = 0;
      #endif
      #ifdef GRPC_CALLBACK_API_NONEXPERIMENTAL
      virtual void GetSign(::grpc::ClientContext* context, const ::grpc::ByteBuffer* request, ::SignResponse* response, ::grpc::ClientUnaryReactor* reactor) = 0;
      #else
      virtual void GetSign(::grpc::ClientContext* context, const ::grpc::ByteBuffer* request, ::SignResponse* response, ::grpc::experimental::ClientUnaryReactor* reactor) = 0;
      #endif
    };
    #ifdef GRPC_CALLBACK_API_NONEXPERIMENTAL
    typedef class experimental_async_interface async_interface;
    #endif
    #ifdef GRPC_CALLBACK_API_NONEXPERIMENTAL
    async_interface* async() { return experimental_async(); }
    #endif
    virtual class experimental_async_interface* experimental_async() { return nullptr; }
  private:
    virtual ::grpc::ClientAsyncResponseReaderInterface< ::SignResponse>* AsyncGetSignRaw(::grpc::ClientContext* context, const ::SignRequest& request, ::grpc::CompletionQueue* cq) = 0;
    virtual ::grpc::ClientAsyncResponseReaderInterface< ::SignResponse>* PrepareAsyncGetSignRaw(::grpc::ClientContext* context, const ::SignRequest& request, ::grpc::CompletionQueue* cq) = 0;
  };
  class Stub final : public StubInterface {
   public:
    Stub(const std::shared_ptr< ::grpc::ChannelInterface>& channel);
    ::grpc::Status GetSign(::grpc::ClientContext* context, const ::SignRequest& request, ::SignResponse* response) override;
    std::unique_ptr< ::grpc::ClientAsyncResponseReader< ::SignResponse>> AsyncGetSign(::grpc::ClientContext* context, const ::SignRequest& request, ::grpc::CompletionQueue* cq) {
      return std::unique_ptr< ::grpc::ClientAsyncResponseReader< ::SignResponse>>(AsyncGetSignRaw(context, request, cq));
    }
    std::unique_ptr< ::grpc::ClientAsyncResponseReader< ::SignResponse>> PrepareAsyncGetSign(::grpc::ClientContext* context, const ::SignRequest& request, ::grpc::CompletionQueue* cq) {
      return std::unique_ptr< ::grpc::ClientAsyncResponseReader< ::SignResponse>>(PrepareAsyncGetSignRaw(context, request, cq));
    }
    class experimental_async final :
      public StubInterface::experimental_async_interface {
     public:
      void GetSign(::grpc::ClientContext* context, const ::SignRequest* request, ::SignResponse* response, std::function<void(::grpc::Status)>) override;
      void GetSign(::grpc::ClientContext* context, const ::grpc::ByteBuffer* request, ::SignResponse* response, std::function<void(::grpc::Status)>) override;
      #ifdef GRPC_CALLBACK_API_NONEXPERIMENTAL
      void GetSign(::grpc::ClientContext* context, const ::SignRequest* request, ::SignResponse* response, ::grpc::ClientUnaryReactor* reactor) override;
      #else
      void GetSign(::grpc::ClientContext* context, const ::SignRequest* request, ::SignResponse* response, ::grpc::experimental::ClientUnaryReactor* reactor) override;
      #endif
      #ifdef GRPC_CALLBACK_API_NONEXPERIMENTAL
      void GetSign(::grpc::ClientContext* context, const ::grpc::ByteBuffer* request, ::SignResponse* response, ::grpc::ClientUnaryReactor* reactor) override;
      #else
      void GetSign(::grpc::ClientContext* context, const ::grpc::ByteBuffer* request, ::SignResponse* response, ::grpc::experimental::ClientUnaryReactor* reactor) override;
      #endif
     private:
      friend class Stub;
      explicit experimental_async(Stub* stub): stub_(stub) { }
      Stub* stub() { return stub_; }
      Stub* stub_;
    };
    class experimental_async_interface* experimental_async() override { return &async_stub_; }

   private:
    std::shared_ptr< ::grpc::ChannelInterface> channel_;
    class experimental_async async_stub_{this};
    ::grpc::ClientAsyncResponseReader< ::SignResponse>* AsyncGetSignRaw(::grpc::ClientContext* context, const ::SignRequest& request, ::grpc::CompletionQueue* cq) override;
    ::grpc::ClientAsyncResponseReader< ::SignResponse>* PrepareAsyncGetSignRaw(::grpc::ClientContext* context, const ::SignRequest& request, ::grpc::CompletionQueue* cq) override;
    const ::grpc::internal::RpcMethod rpcmethod_GetSign_;
  };
  static std::unique_ptr<Stub> NewStub(const std::shared_ptr< ::grpc::ChannelInterface>& channel, const ::grpc::StubOptions& options = ::grpc::StubOptions());

  class Service : public ::grpc::Service {
   public:
    Service();
    virtual ~Service();
    virtual ::grpc::Status GetSign(::grpc::ServerContext* context, const ::SignRequest* request, ::SignResponse* response);
  };
  template <class BaseClass>
  class WithAsyncMethod_GetSign : public BaseClass {
   private:
    void BaseClassMustBeDerivedFromService(const Service* /*service*/) {}
   public:
    WithAsyncMethod_GetSign() {
      ::grpc::Service::MarkMethodAsync(0);
    }
    ~WithAsyncMethod_GetSign() override {
      BaseClassMustBeDerivedFromService(this);
    }
    // disable synchronous version of this method
    ::grpc::Status GetSign(::grpc::ServerContext* /*context*/, const ::SignRequest* /*request*/, ::SignResponse* /*response*/) override {
      abort();
      return ::grpc::Status(::grpc::StatusCode::UNIMPLEMENTED, "");
    }
    void RequestGetSign(::grpc::ServerContext* context, ::SignRequest* request, ::grpc::ServerAsyncResponseWriter< ::SignResponse>* response, ::grpc::CompletionQueue* new_call_cq, ::grpc::ServerCompletionQueue* notification_cq, void *tag) {
      ::grpc::Service::RequestAsyncUnary(0, context, request, response, new_call_cq, notification_cq, tag);
    }
  };
  typedef WithAsyncMethod_GetSign<Service > AsyncService;
  template <class BaseClass>
  class ExperimentalWithCallbackMethod_GetSign : public BaseClass {
   private:
    void BaseClassMustBeDerivedFromService(const Service* /*service*/) {}
   public:
    ExperimentalWithCallbackMethod_GetSign() {
    #ifdef GRPC_CALLBACK_API_NONEXPERIMENTAL
      ::grpc::Service::
    #else
      ::grpc::Service::experimental().
    #endif
        MarkMethodCallback(0,
          new ::grpc_impl::internal::CallbackUnaryHandler< ::SignRequest, ::SignResponse>(
            [this](
    #ifdef GRPC_CALLBACK_API_NONEXPERIMENTAL
                   ::grpc::CallbackServerContext*
    #else
                   ::grpc::experimental::CallbackServerContext*
    #endif
                     context, const ::SignRequest* request, ::SignResponse* response) { return this->GetSign(context, request, response); }));}
    void SetMessageAllocatorFor_GetSign(
        ::grpc::experimental::MessageAllocator< ::SignRequest, ::SignResponse>* allocator) {
    #ifdef GRPC_CALLBACK_API_NONEXPERIMENTAL
      ::grpc::internal::MethodHandler* const handler = ::grpc::Service::GetHandler(0);
    #else
      ::grpc::internal::MethodHandler* const handler = ::grpc::Service::experimental().GetHandler(0);
    #endif
      static_cast<::grpc_impl::internal::CallbackUnaryHandler< ::SignRequest, ::SignResponse>*>(handler)
              ->SetMessageAllocator(allocator);
    }
    ~ExperimentalWithCallbackMethod_GetSign() override {
      BaseClassMustBeDerivedFromService(this);
    }
    // disable synchronous version of this method
    ::grpc::Status GetSign(::grpc::ServerContext* /*context*/, const ::SignRequest* /*request*/, ::SignResponse* /*response*/) override {
      abort();
      return ::grpc::Status(::grpc::StatusCode::UNIMPLEMENTED, "");
    }
    #ifdef GRPC_CALLBACK_API_NONEXPERIMENTAL
    virtual ::grpc::ServerUnaryReactor* GetSign(
      ::grpc::CallbackServerContext* /*context*/, const ::SignRequest* /*request*/, ::SignResponse* /*response*/)
    #else
    virtual ::grpc::experimental::ServerUnaryReactor* GetSign(
      ::grpc::experimental::CallbackServerContext* /*context*/, const ::SignRequest* /*request*/, ::SignResponse* /*response*/)
    #endif
      { return nullptr; }
  };
  #ifdef GRPC_CALLBACK_API_NONEXPERIMENTAL
  typedef ExperimentalWithCallbackMethod_GetSign<Service > CallbackService;
  #endif

  typedef ExperimentalWithCallbackMethod_GetSign<Service > ExperimentalCallbackService;
  template <class BaseClass>
  class WithGenericMethod_GetSign : public BaseClass {
   private:
    void BaseClassMustBeDerivedFromService(const Service* /*service*/) {}
   public:
    WithGenericMethod_GetSign() {
      ::grpc::Service::MarkMethodGeneric(0);
    }
    ~WithGenericMethod_GetSign() override {
      BaseClassMustBeDerivedFromService(this);
    }
    // disable synchronous version of this method
    ::grpc::Status GetSign(::grpc::ServerContext* /*context*/, const ::SignRequest* /*request*/, ::SignResponse* /*response*/) override {
      abort();
      return ::grpc::Status(::grpc::StatusCode::UNIMPLEMENTED, "");
    }
  };
  template <class BaseClass>
  class WithRawMethod_GetSign : public BaseClass {
   private:
    void BaseClassMustBeDerivedFromService(const Service* /*service*/) {}
   public:
    WithRawMethod_GetSign() {
      ::grpc::Service::MarkMethodRaw(0);
    }
    ~WithRawMethod_GetSign() override {
      BaseClassMustBeDerivedFromService(this);
    }
    // disable synchronous version of this method
    ::grpc::Status GetSign(::grpc::ServerContext* /*context*/, const ::SignRequest* /*request*/, ::SignResponse* /*response*/) override {
      abort();
      return ::grpc::Status(::grpc::StatusCode::UNIMPLEMENTED, "");
    }
    void RequestGetSign(::grpc::ServerContext* context, ::grpc::ByteBuffer* request, ::grpc::ServerAsyncResponseWriter< ::grpc::ByteBuffer>* response, ::grpc::CompletionQueue* new_call_cq, ::grpc::ServerCompletionQueue* notification_cq, void *tag) {
      ::grpc::Service::RequestAsyncUnary(0, context, request, response, new_call_cq, notification_cq, tag);
    }
  };
  template <class BaseClass>
  class ExperimentalWithRawCallbackMethod_GetSign : public BaseClass {
   private:
    void BaseClassMustBeDerivedFromService(const Service* /*service*/) {}
   public:
    ExperimentalWithRawCallbackMethod_GetSign() {
    #ifdef GRPC_CALLBACK_API_NONEXPERIMENTAL
      ::grpc::Service::
    #else
      ::grpc::Service::experimental().
    #endif
        MarkMethodRawCallback(0,
          new ::grpc_impl::internal::CallbackUnaryHandler< ::grpc::ByteBuffer, ::grpc::ByteBuffer>(
            [this](
    #ifdef GRPC_CALLBACK_API_NONEXPERIMENTAL
                   ::grpc::CallbackServerContext*
    #else
                   ::grpc::experimental::CallbackServerContext*
    #endif
                     context, const ::grpc::ByteBuffer* request, ::grpc::ByteBuffer* response) { return this->GetSign(context, request, response); }));
    }
    ~ExperimentalWithRawCallbackMethod_GetSign() override {
      BaseClassMustBeDerivedFromService(this);
    }
    // disable synchronous version of this method
    ::grpc::Status GetSign(::grpc::ServerContext* /*context*/, const ::SignRequest* /*request*/, ::SignResponse* /*response*/) override {
      abort();
      return ::grpc::Status(::grpc::StatusCode::UNIMPLEMENTED, "");
    }
    #ifdef GRPC_CALLBACK_API_NONEXPERIMENTAL
    virtual ::grpc::ServerUnaryReactor* GetSign(
      ::grpc::CallbackServerContext* /*context*/, const ::grpc::ByteBuffer* /*request*/, ::grpc::ByteBuffer* /*response*/)
    #else
    virtual ::grpc::experimental::ServerUnaryReactor* GetSign(
      ::grpc::experimental::CallbackServerContext* /*context*/, const ::grpc::ByteBuffer* /*request*/, ::grpc::ByteBuffer* /*response*/)
    #endif
      { return nullptr; }
  };
  template <class BaseClass>
  class WithStreamedUnaryMethod_GetSign : public BaseClass {
   private:
    void BaseClassMustBeDerivedFromService(const Service* /*service*/) {}
   public:
    WithStreamedUnaryMethod_GetSign() {
      ::grpc::Service::MarkMethodStreamed(0,
        new ::grpc::internal::StreamedUnaryHandler< ::SignRequest, ::SignResponse>(std::bind(&WithStreamedUnaryMethod_GetSign<BaseClass>::StreamedGetSign, this, std::placeholders::_1, std::placeholders::_2)));
    }
    ~WithStreamedUnaryMethod_GetSign() override {
      BaseClassMustBeDerivedFromService(this);
    }
    // disable regular version of this method
    ::grpc::Status GetSign(::grpc::ServerContext* /*context*/, const ::SignRequest* /*request*/, ::SignResponse* /*response*/) override {
      abort();
      return ::grpc::Status(::grpc::StatusCode::UNIMPLEMENTED, "");
    }
    // replace default version of method with streamed unary
    virtual ::grpc::Status StreamedGetSign(::grpc::ServerContext* context, ::grpc::ServerUnaryStreamer< ::SignRequest,::SignResponse>* server_unary_streamer) = 0;
  };
  typedef WithStreamedUnaryMethod_GetSign<Service > StreamedUnaryService;
  typedef Service SplitStreamedService;
  typedef WithStreamedUnaryMethod_GetSign<Service > StreamedService;
};


#endif  // GRPC_SpringSignOperation_2eproto__INCLUDED
