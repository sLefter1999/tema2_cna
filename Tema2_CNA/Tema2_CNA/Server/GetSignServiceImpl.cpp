#include "GetSignServiceImpl.h"
#include "WinterSignOperation.grpc.pb.h"
#include "SignsDB.h"

#include "WinterSignServiceImpl.h"
#include "SpringSignServiceImpl.h"
#include "SummerSignServiceImpl.h"
#include "AutumnSignServiceImpl.h"

#include <sstream>
#include <iostream>

#include <grpc/grpc.h>
#include <grpcpp/channel.h>
#include <grpcpp/client_context.h>
#include <grpcpp/create_channel.h>
#include <grpcpp/security/credentials.h>

#include <grpc/grpc.h>
#include <grpcpp/server.h>
#include <grpcpp/server_builder.h>
#include <grpcpp/server_context.h>

using grpc::Channel;
using grpc::ClientContext;
using grpc::ClientReader;
using grpc::ClientReaderWriter;
using grpc::ClientWriter;

::grpc::Status GetSignServiceImpl::GetSign(::grpc::ServerContext* context, const::SignRequest* request, ::SignResponse* response)
{

	std::shared_ptr<grpc_impl::Channel> channel = grpc::CreateChannel("localhost:8887",grpc::InsecureChannelCredentials());
	
	ClientContext clientContext;
	grpc::Status status;

	switch (GetSeason(request->birthdate()))
	{
	case Season::WINTER:
	{
		auto winter_sign_stub = GetWinterSignService::NewStub(channel);
		status = winter_sign_stub->GetSign(&clientContext,*request,response);
	}
	break;

	case Season::SPRING:
	{
		auto spring_sign_stub = GetSpringSignService::NewStub(channel);
		status = spring_sign_stub->GetSign(&clientContext,*request,response);
	}
	break;

	case Season::SUMMER:
	{
		auto summer_sign_stub = GetSummerSignService::NewStub(channel);
		status = summer_sign_stub->GetSign(&clientContext,*request,response);
	}
	break;

	case Season::AUTUMN:
	{
		auto autumn_sign_stub = GetAutumnSignService::NewStub(channel);
		status = autumn_sign_stub->GetSign(&clientContext,*request,response);
	}

	default:break;
	}

	return status;
}

void GetSignServiceImpl::StartServer()
{
	grpc_init();

	std::string server_address("localhost:8887");
	WinterSignServiceImpl winter_service;
	SpringSignServiceImpl spring_service;
	SummerSignServiceImpl summer_service;
	AutumnSignServiceImpl autumn_service;

	::grpc_impl::ServerBuilder serverBuilder;
	serverBuilder.AddListeningPort(server_address, grpc::InsecureServerCredentials());

	serverBuilder.RegisterService(&winter_service);
	serverBuilder.RegisterService(&spring_service);
	serverBuilder.RegisterService(&summer_service);
	serverBuilder.RegisterService(&autumn_service);

	std::unique_ptr<::grpc_impl::Server> server(serverBuilder.BuildAndStart());

	std::cout << "Server listening on " << server_address << std::endl;

	server->Wait();
}

Season GetSignServiceImpl::GetSeason(const std::string& birthdate)
{
	std::stringstream buffer(birthdate);

	std::string date;

	buffer >> date;
	int day = std::stoi(date);

	buffer >> date;
	int month = std::stoi(date);

	buffer >> date;
	int year = std::stoi(date);

	if (month <= 2 || month == 12)
		return Season::WINTER;
	else if (month <= 5)
		return Season::SPRING;
	else if (month <= 8)
		return Season::SUMMER;
	else return Season::AUTUMN;
}
