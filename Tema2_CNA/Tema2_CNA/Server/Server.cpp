#include <iostream>
//#include"AdditionServiceImpl.h"

#include<grpc/grpc.h>
#include<grpcpp/server.h>
#include<grpcpp/server_builder.h>
#include<grpcpp/server_context.h>

#include"GetSignServiceImpl.h"
#include"WinterSignServiceImpl.h"
#include"SpringSignServiceImpl.h"

#include"SignsDB.h"

SignsDB* SignsDB::signsDb = nullptr;

int main()
{
    std::string server_address("localhost:8888");
    GetSignServiceImpl service;

    ::grpc_impl::ServerBuilder serverBuilder;
    serverBuilder.AddListeningPort(server_address, grpc::InsecureServerCredentials());
    serverBuilder.RegisterService(&service);

    std::unique_ptr<::grpc_impl::Server> server(serverBuilder.BuildAndStart());

    SignsDB* signsDb = SignsDB::GetInstance();
    signsDb->ParseSigns("Signs.txt");

    std::cout << "Loaded zodiac signs..." << std::endl;

    std::cout << "Server listening on" << server_address << std::endl;

    service.StartServer();

    server->Wait();
}

