#include "WinterSignServiceImpl.h"

#include "SignsDB.h"

#include <sstream>

::grpc::Status WinterSignServiceImpl::GetSign(::grpc::ServerContext* context, const::SignRequest* request, ::SignResponse* response)
{
	SignsDB* signsDb = SignsDB::GetInstance();

	Date client_date = Date::ParseDate(request->birthdate());

	for (int index = 0; index < 4; index++)
	{
		Sign sign = signsDb->GetSigns()[index];

		if (Date::IsDateInRange(client_date.day, client_date.month, sign.GetFromDate().day, sign.GetFromDate().month, sign.GetToDate().day, sign.GetToDate().month))
		{
			response->set_sign(sign.GetSignName());
			break;
		}
	}

	if (response->sign() == "")
		response->set_sign("Capricorn");

	return ::grpc::Status::OK;
}
