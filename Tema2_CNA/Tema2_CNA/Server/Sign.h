#pragma once
#include <string>

struct Date
{
	Date(int day, int month) :day(day),month(month) {};

	int day;
	int month;

	static Date ParseDate(const std::string& date);
	static bool IsDateInRange(int day, int month, int startDay, int startMonth, int endDay, int endMonth);
};


class Sign
{
public:
	Sign(std::string signName, Date from, Date to) :
		sign(signName),
		from(from),
		to(to) {};

	std::string GetSignName();
	Date GetFromDate();
	Date GetToDate();
private:
	std::string sign;
	Date from;
	Date to;
};
