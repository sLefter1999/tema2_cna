#pragma once
#include "SummerSignOperation.grpc.pb.h"

#include <string>

class SummerSignServiceImpl final : public GetSummerSignService::Service
{
public:
	::grpc::Status GetSign(::grpc::ServerContext* context, const ::SignRequest* request, ::SignResponse* response) override;
};