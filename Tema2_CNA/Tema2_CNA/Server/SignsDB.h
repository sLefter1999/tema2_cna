#pragma once
#include <vector>

#include "Sign.h"

class SignsDB
{
public:
	static SignsDB* GetInstance();
	void ParseSigns(const std::string& filePath);
	std::string GetSign(Date date);
	std::vector<Sign> GetSigns();

private:
	SignsDB() {};
	static SignsDB* signsDb;
	std::vector<Sign> signs;
};