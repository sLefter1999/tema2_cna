#include "SpringSignServiceImpl.h"

#include "SignsDB.h"

::grpc::Status SpringSignServiceImpl::GetSign(::grpc::ServerContext* context, const::SignRequest* request, ::SignResponse* response)
{
	SignsDB* signsDb = SignsDB::GetInstance();

	Date client_date = Date::ParseDate(request->birthdate());

	for (int index = 3; index < 7; index++)
	{
		Sign sign = signsDb->GetSigns()[index];

		if (Date::IsDateInRange(client_date.day, client_date.month, sign.GetFromDate().day, sign.GetFromDate().month, sign.GetToDate().day, sign.GetToDate().month))
		{
			response->set_sign(sign.GetSignName());
			break;
		}
	}

	return ::grpc::Status::OK;
}
