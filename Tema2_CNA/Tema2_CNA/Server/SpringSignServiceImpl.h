#pragma once
#include "SpringSignOperation.grpc.pb.h"

#include <string>

class SpringSignServiceImpl final : public GetSpringSignService::Service
{
public:
	::grpc::Status GetSign(::grpc::ServerContext* context, const ::SignRequest* request, ::SignResponse* response) override;
};