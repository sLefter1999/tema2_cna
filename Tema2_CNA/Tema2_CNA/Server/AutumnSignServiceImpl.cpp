#include "AutumnSignServiceImpl.h"

#include "SignsDB.h"

::grpc::Status AutumnSignServiceImpl::GetSign(::grpc::ServerContext* context, const::SignRequest* request, ::SignResponse* response)
{
	SignsDB* signsDb = SignsDB::GetInstance();

	Date client_date = Date::ParseDate(request->birthdate());

	for (int index = 9; index <12; index++)
	{
		Sign sign = signsDb->GetSigns()[index];

		if (Date::IsDateInRange(client_date.day, client_date.month, sign.GetFromDate().day, sign.GetFromDate().month, sign.GetToDate().day, sign.GetToDate().month))
		{
			response->set_sign(sign.GetSignName());
			break;
		}
	}

	Sign sign = signsDb->GetSigns()[0];

	if (Date::IsDateInRange(client_date.day, client_date.month, sign.GetFromDate().day, sign.GetFromDate().month, sign.GetToDate().day, sign.GetToDate().month))
	{
		response->set_sign(sign.GetSignName());
	}

	return ::grpc::Status::OK;
}
