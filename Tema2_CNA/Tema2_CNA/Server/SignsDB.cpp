#include "SignsDB.h"

#include <fstream>
#include <sstream>

SignsDB* SignsDB::GetInstance()
{
	if (signsDb == 0)
		signsDb = new SignsDB();

	 return signsDb;
}

void SignsDB::ParseSigns(const std::string& filePath)
{
	std::ifstream file(filePath);

	std::string line;

	while (file>>line)
	{
		std::string signName = line;

		std::string date;
		file >> date;

		std::stringstream buffer1(date);

		std::getline(buffer1, date, '/');
		int firstDay = std::stoi(date);

		std::getline(buffer1, date, '/');
		int firstMonth = std::stoi(date);

		file >> date;

		std::stringstream buffer2(date);

		std::getline(buffer2, date, '/');
		int secondDay = std::stoi(date);

		std::getline(buffer2, date, '/');
		int secondMonth = std::stoi(date);

		signs.push_back(Sign(signName, Date(firstDay,firstMonth), Date(secondDay,secondMonth)));
	}
}

std::string SignsDB::GetSign(Date date)
{
	for (int index = 0; index < signs.size(); index++)
		if (Date::IsDateInRange(date.day, date.month, signs[index].GetFromDate().day, signs[index].GetFromDate().month, signs[index].GetToDate().day, signs[index].GetToDate().month))
			return signs[index].GetSignName();

	return "Capricorn";
}

std::vector<Sign> SignsDB::GetSigns()
{
	return this->signs;
}

