#pragma once
#include "AutumnSignOperation.grpc.pb.h"

#include <string>

class AutumnSignServiceImpl final : public GetAutumnSignService::Service
{
public:
	::grpc::Status GetSign(::grpc::ServerContext* context, const ::SignRequest* request, ::SignResponse* response) override;
};