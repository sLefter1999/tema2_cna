#include "Sign.h"

#include <sstream>
#include <string>

std::string Sign::GetSignName()
{
	return sign;
}

Date Sign::GetFromDate()
{
	return from;
}

Date Sign::GetToDate()
{
	return to;
}

Date Date::ParseDate(const std::string& date)
{
	std::istringstream buffer(date);

	std::string line;

	std::getline(buffer, line, '/');
	int month = std::stoi(line);

	std::getline(buffer, line, '/');
	int day = std::stoi(line);

	return Date(day,month);
}

bool Date::IsDateInRange(int day, int month, int startDay, int startMonth, int endDay, int endMonth)
{
	int entryDate = (month * 100) + day;
	int startDate = (startMonth * 100) + startDay;
	int endDate = (endMonth * 100) + endDay;

	if (entryDate >= startDate && entryDate <= endDate) {
		return true;
	}
	else {
		return false;
	}
}
