#pragma once
#include "SignOperation.grpc.pb.h"

#include <string>

enum class Season
{
	WINTER,
	SPRING,
	SUMMER,
	AUTUMN
};

class GetSignServiceImpl final : public GetSignService::Service
{
public:
	::grpc::Status GetSign(::grpc::ServerContext* context, const ::SignRequest* request, ::SignResponse* response) override;

	void StartServer();
private:
	Season GetSeason(const std::string& birthdate);
};