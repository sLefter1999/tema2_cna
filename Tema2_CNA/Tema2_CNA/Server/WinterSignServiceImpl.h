#pragma once
#include "WinterSignOperation.grpc.pb.h"

#include <string>

class WinterSignServiceImpl final : public GetWinterSignService::Service
{
public:
	::grpc::Status GetSign(::grpc::ServerContext* context, const ::SignRequest* request, ::SignResponse* response) override;
};